import test from 'ava'
import moment from 'moment'
import { _testonly, resolveColumnName } from '~/util/bulk-columns'

const sdp = 'sighting date processor'

test(`${sdp} - is a numerical Excel date processed correctly?`, (t) => {
  const val = 42788 // 2017-02-22 in Adelaide timezone, ...-21 in UTC
  const objectUnderTest = _testonly.sightingDateProcessor
  const result = objectUnderTest(val)
  t.true(moment(result).isSame('2017-02-22', 'day'))
})

test(`${sdp} - is a year-month-day string date processed correctly?`, (t) => {
  const val = '2018-09-17'
  const objectUnderTest = _testonly.sightingDateProcessor
  const result = objectUnderTest(val)
  t.true(moment(result).isSame('2018-09-17', 'day'))
})

test(`${sdp} - is a year-month string date processed correctly?`, (t) => {
  const val = '2018-04'
  const objectUnderTest = _testonly.sightingDateProcessor
  const result = objectUnderTest(val)
  t.true(moment(result).isSame('2018-04-01', 'day'))
})

test(`${sdp} - is a year-only string date processed correctly?`, (t) => {
  const val = '2018'
  const objectUnderTest = _testonly.sightingDateProcessor
  const origConsoleWarn = console.warn
  console.warn = () => {} // silence non-RFC2822/ISO date format console output
  const result = objectUnderTest(val)
  console.warn = origConsoleWarn
  t.true(moment(result).isSame('2018-01-01', 'day'))
})

test(`${sdp} - is an unsupported type handled nicely?`, (t) => {
  const val = {foo: 'an object'}
  const objectUnderTest = _testonly.sightingDateProcessor
  try {
    objectUnderTest(val)
  } catch (err) {
    const expected = 'Value type error:'
    t.is(err.message.substr(0, expected.length), expected)
  }
})

const rcn = 'resolveColumnName '

test(`${rcn} - can we resolve a field that exists?`, (t) => {
  const fieldName = 'orchid_type'
  const result = resolveColumnName(fieldName)
  t.is(result, 'Orchid Type')
})

test(`${rcn} - can we handle a field that we don't know about?`, (t) => {
  const fieldName = 'some_new_field'
  const result = resolveColumnName(fieldName)
  t.is(result, 'some_new_field')
})
