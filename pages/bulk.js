import XlsxPopulate from 'xlsx-populate/browser/xlsx-populate.js'
import moment from 'moment'
import uuid from 'uuid'

import FileInput from '~/components/FileInput'
import WowLogging from '~/mixins/WowLogging'
import { pageTitle } from '~/util/helpers'
import { colTypes, columns, cleanName, resolveColumnName } from '~/util/bulk-columns'

const uuidMarker = '!UUID:'

export default {
  head: pageTitle('Legacy data bulk upload'),
  data () {
    return {
      uiState: 'initial',
      completedForm: null,
      validationFailures: [],
      rowsProcessed: 0,
      currentUploadActivity: '',
    }
  },
  components: {
    FileInput,
  },
  computed: {
    isUploadDisabled () {
      return !this.completedForm
    },
  },
  middleware: ['auth'],
  mixins: [WowLogging],
  methods: {
    async onCompletedTemplateSelected (file) {
      this.completedForm = file
    },
    async downloadTemplate () {
      const workbook = await XlsxPopulate.fromBlankAsync()
      populateWorkbook(workbook)
      const workbookBytes = await workbook.outputAsync()
      triggerDownload(workbookBytes, 'wild-orchid-watch-bulk-data-template.xlsx')
    },
    async uploadTemplate () {
      this.uiState = 'processing'
      this.validationFailures = []
      this.currentUploadActivity = ''
      let workbook
      try {
        workbook = await XlsxPopulate.fromDataAsync(this.completedForm)
      } catch (err) {
        this.consoleError(`Failed to read spreadsheet`, err)
        this.uiState = 'parseFail'
        return
      }
      try {
        await this.workbookProcessor(workbook, this.validationFailures)
        if (this.validationFailures.length) {
          this.uiState = 'processFail'
          const workbookBase64 = await workbook.outputAsync('base64')
          this.rollbar.log('Form upload failed due to validation failures', {
            validationFailures: this.validationFailures,
            workbook: workbookBase64
          })
          return
        }
        // FIXME store complete workbook somewhere so we can always reprocess
        this.uiState = 'success'
      } catch (err) {
        this.uiState = 'systemFail'
        this.consoleError(this.chainedError(err, `Failed while processing workbook`))
      }
    },
    async workbookProcessor (workbook, validationFailures) {
      this.rowsProcessed = 0
      const sheet = workbook.sheet("Sheet1")
      const bulkRecordPk = await this.validateUuidAndGetRecordId(sheet, validationFailures)
      if (validationFailures.length) {
        return
      }
      const usedRange = sheet.usedRange()
      const startCell = usedRange.startCell()
      const endCell = usedRange.endCell()
      const endRowNum = endCell.rowNumber()
      const rawRows = usedRange.value().slice(1)
      const records = []
      let currRowNum = 2
      for (const currRawRow of rawRows) {
        console.debug(`Processing row ${currRowNum}`)
        this.currentUploadActivity = `Validating row ${currRowNum}`
        // TODO check for completely empty row and continue
        const isRowEmpty = currRawRow.every(e => !e)
        if (isRowEmpty) {
          console.debug(`Skipping row ${currRowNum} as it's completely empty`)
          continue
        }
        const record = this.validateAndExtractRow(currRawRow, validationFailures)
        record.bulk_parent = bulkRecordPk
        records.push(record)
        currRowNum++
        this.rowsProcessed++
      }
      const isFailures = validationFailures.length > 0
      if (isFailures) {
        return
      }
      // TODO add loading bar that tracks upload progress
      currRowNum = 2
      for (const curr of records) { // TODO go parallel
        this.currentUploadActivity = `Uploading row ${currRowNum}`
        const opts = { // FIXME extract header setting to central spot
          headers: { accept: 'application/json; version=1' }
        }
        try {
          await this.$axios.post('wowbulkitems/', curr, opts)
        } catch (err) {
          const statusCode = err.response && err.response.status
          const body = err.response.data
          if (statusCode === 400) {
            for (const currKey of Object.keys(body)) {
              const fieldName = resolveColumnName(currKey)
              this.validationFailures.push(`Row ${currRowNum}, column '${fieldName}': ${body[currKey]}`)
            }
            continue
          }
          this.consoleError('Failed to upload bulk item', err)
        } finally {
          currRowNum++
        }
      }
      // FIXME trigger or flag census list refresh in Vuex store
    },
    validateAndExtractRow (raw, validationFailures) {
      const result = {}
      let fieldIndex = 0
      for (const currFieldDef of columns) {
        const value = raw[fieldIndex]
        fieldIndex++
        if (['undefined', 'null'].indexOf(typeof(value)) >= 0 && currFieldDef.optional) {
          console.debug(`Skipping field '${cleanName(currFieldDef.name)}' as it's not supplied and not required`)
          continue
        }
        const basicValidator = currFieldDef.type
        const validationResult = basicValidator(value)
        if (validationResult !== true) {
          validationFailures.push(validationResult)
          continue
        }
        if (currFieldDef.processor) {
          result[currFieldDef.fieldName] = currFieldDef.processor(value)
          continue
        }
        result[currFieldDef.fieldName] = currFieldDef.uppercase ? value.toUpperCase() : value
      }
      return result
    },
    async validateUuidAndGetRecordId (sheet, validationFailures) {
      const downloadFreshFragment = 'downloading a fresh, empty template and copying your data over. Then upload that new spreadsheet.'
      const tryFreshTemplateMsg = 'The uploaded spreadsheet is missing required metadata. Try ' + downloadFreshFragment
      const found = sheet.find(uuidMarker)
      if (found.length !== 1) {
        this.consoleError(new Error(`Failed to find UUID marker '${uuidMarker}'. Expected exactly 1 instance, found ${found.length}.`))
        validationFailures.push(tryFreshTemplateMsg)
        return
      }
      const uuidCell = found[0]
      const rawValue = uuidCell.value()
      const uuidValue = rawValue.replace(uuidMarker, '')
      if (!uuidValue) {
        console.error(new Error(`Extracted UUID value='${uuidValue}' doesn't look right, cannot continue.`)) // don't send to Rollbar
        validationFailures.push(tryFreshTemplateMsg)
        return
      }
      try {
        const resp = await this.$axios.get(`wowbulks/`, {
          params: {
            'filter{bulk_id}': uuidValue,
          }
        })
        const matchCount = resp.data.meta.total_results
        if (matchCount > 1) {
          this.consoleWarn(`Warning: bulk_id should be unique but found '${matchCount}' for '${uuidValue}'`)
        }
        if (!matchCount) {
          return this.createBulkRecord(uuidValue)
        }
        const pk = resp.data.wowbulks[0].id
        await this.$axios.post(`wowbulks/${pk}/reset/`)
        return pk
        return
      } catch (err) {
        const statusCode = err.response && err.response.status
        const isRecordCannotBeReset = false // FIXME trap status code for "record exists but cannot be reset"
        if (isRecordCannotBeReset) {
          validationFailures.push('This data has already been uploaded and is pending approval. If you think this is a' +
            'mistake, you can try ' + downloadFreshFragment)
          return
        }
        throw err
      }
    },
    async createBulkRecord (bulkId) {
      const resp = await this.$axios.post('wowbulks/', { bulk_id: bulkId })
      return resp.data.wowbulk.id
    },
  },
}

function populateWorkbook (workbook) {
  const columnLabels = columns.map(e => e.name)
  columnLabels.push(uuidMarker + uuid()) // cols are 0-indexed, so we have space for the UUID
  const endColNum = columnLabels.length
  const sheet = workbook.sheet(0)
  const endColName = sheet.column(endColNum).columnName()
  sheet.range(`A1:${endColName}1`).value([columnLabels])
  sheet.row(1).height(50)
  sheet.cell(`${endColName}1`)
    .style('fontSize', 1)
    .style('fontColor', 'FFFFFF')
  let currColNum = 0
  for (const curr of columns) {
    const width = curr.width
    currColNum++
    if (width) {
      sheet.column(currColNum).width(width)
    }
    // FIXME add formatting for different types
  }
}

function triggerDownload (blob, filename) {
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, filename)
    return
  }
  const url = window.URL.createObjectURL(blob)
  const a = document.createElement('a')
  document.body.appendChild(a)
  a.href = url
  a.download = filename
  a.click()
  window.URL.revokeObjectURL(url)
  document.body.removeChild(a)
}

