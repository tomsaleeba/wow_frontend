export const state = () => ({
  isProd: false,
  censusList: [],
})

export const mutations = {
  UPDATE_CENSUS_LIST: function (state, apiResp) {
    state.censusList = apiResp
  },
  SAVE_NEW_CENSUS: function (state, newCensus) {
    state.censusList.push(newCensus)
  },
  SET_IS_PROD: function (state, isIt) {
    state.isProd = isIt
  },
}

export const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  nuxtServerInit ({ commit }, { }) {
    const isProd = this.$env.DEPLOYED_TO_ENV === 'production'
    commit('SET_IS_PROD', isProd)
  },

  async listCensuses ({ commit, getters }) {
    const { data } = await this.$axios.get(`census/`) // FIXME handle paging
    commit('UPDATE_CENSUS_LIST', data.census)
  },

  async addCensus ({ commit, getters }, { newCensus }) {
    const { data } = await this.$axios.post(`census/`, newCensus, { headers: headers })
    commit('SAVE_NEW_CENSUS', data.census)
  },
}
