import { isJustLoggedOutParamName } from './constants.js'

export async function doLogout (componentThis) {
  await componentThis.$auth.logout()
  const currentUrl= encodeURIComponent(`${window.location.origin}/?${isJustLoggedOutParamName}=1`)
  const keycloakHost = componentThis.$env.KEYCLOAK_BASE_URL
  const keycloakRealm = componentThis.$env.KEYCLOAK_REALM
  window.location = `${keycloakHost}/auth/realms/${keycloakRealm}/protocol/openid-connect/logout?redirect_uri=${currentUrl}`
}

export function pageTitle (title) {
  return {
    title: title + ' - Wild Orchid Watch'
  }
}
