import moment from 'moment'
import XlsxPopulate from 'xlsx-populate/browser/xlsx-populate.js'

// FIXME implement these
export const colTypes = {
  date: v => true,
  datum: v => {
    const validValues = ['WGS84']
    return validValues.indexOf(v.toUpperCase()) >= 0 || 'Datum must be GDA94'
  },
  lat: v => true,
  lon: v => true,
  orchidType: v => {
    const validValues = ['TER', 'EPI', 'LIT'] // FIXME pull from API vocabs
    const msg = `Orchid type must be one of ${JSON.stringify(validValues)}`
    return validValues.indexOf(v.toUpperCase()) >= 0 || msg
  },
  positiveInt: v => true,
  species: v => true,
  text: v => true,
  url: v => true,
}

export const columns = [
  {
    name: 'Latitude',
    type: colTypes.lat,
    fieldName: 'lat',
  }, {
    name: 'Longitude',
    type: colTypes.lon,
    width: 10,
    fieldName: 'lon',
  }, {
    name: 'Location\nMethod',
    type: colTypes.text,
    fieldName: 'location_method',
  }, {
    name: 'Location\nDatum',
    type: colTypes.datum,
    fieldName: 'location_datum',
  }, {
    name: 'Location\nReliability',
    type: colTypes.positiveInt,
    fieldName: 'location_reliability',
  }, {
    name: 'Sighting\nDate',
    type: colTypes.date,
    fieldName: 'sighting_date',
    processor: sightingDateProcessor,
  }, {
    name: 'Sighting\nDate\nAccuracy',
    type: colTypes.text,
    fieldName: 'sighting_date_accuracy',
  }, {
    name: 'Obs 1\nName',
    type: colTypes.text,
    fieldName: 'obs_1_name',
  }, {
    name: 'Obs 1\nAffiliation',
    type: colTypes.text,
    fieldName: 'obs_1_affiliation',
  },
  {name: 'Obs 1\nIs private', type: colTypes.text, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Obs 2\nName', type: colTypes.text, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Obs 2\nAffiliation', type: colTypes.text, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Obs 2\nIs private', type: colTypes.text, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {
    name: 'Species',
    type: colTypes.species,
    fieldName: 'species',
  }, {
    name: 'Orchid\nType',
    type: colTypes.orchidType,
    uppercase: true,
    fieldName: 'orchid_type',
  }, {
    name: 'Identification',
    type: colTypes.text, width: 12,
    uppercase: true,
    fieldName: 'identification_method',
  }, {
    name: 'Method\nTechnique',
    type: colTypes.text, width: 12,
    uppercase: true,
    fieldName: 'method_technique',
  },
  {name: 'Project\nTitle', type: colTypes.text,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Publication', type: colTypes.text, width: 12,
    processor: function (result, val) { /* FIXME */ } },
  {
    name: 'Number\nObserved',
    type: colTypes.positiveInt,
    fieldName: 'number_of_individuals_recorded',
  },
  {name: 'Phenology', type: colTypes.text, width: 10, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Location\nDescription', type: colTypes.text, width: 10, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Sighting\nComment', type: colTypes.text, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Habitat\nComment', type: colTypes.text, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Host\nTree\nSpecies', type: colTypes.species, optional: true,
    processor: function (result, val) { /* FIXME */ } },
  {name: 'Photo\nURL', type: colTypes.url, optional: true,
    processor: function (result, val) { /* FIXME */ } },
]

function sightingDateProcessor (val) {
  const parsers = {
    number: v => XlsxPopulate.numberToDate(val),
    string: v => v,
  }
  const parser = parsers[typeof(val)]
  if (!parser) {
    throw new Error(`Value type error: unsupported value type '${typeof(val)}'`)
  }
  const dateString = parser(val)
  const parsedVal = moment(dateString) // using "local mode"
  return parsedVal
}

export function resolveColumnName (fieldName) {
  const colDef = columns.find(e => e.fieldName === fieldName)
  if (!colDef) {
    console.warn(`Programmer problem: could not resolve fieldName='${fieldName}' to a title`)
    return fieldName
  }
  return cleanName(colDef.name)
}

export function cleanName (dirtyName) {
  return dirtyName.replace(/\n/g, ' ')
}

export const _testonly = {
  sightingDateProcessor,
}
