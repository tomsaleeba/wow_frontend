> Web client for the Wild Orchid Watch repository

## Quickstart for local dev

  1. clone the repo
  1. install the dependencies

        yarn install

  1. add the following localhost aliases to your `/etc/hosts` file because the nginx reverse proxy in the wow_backend
     routes based on host

        127.0.0.1	keycloak.wow.local
        127.0.0.1	api.wow.local
        127.0.0.1	s3.wow.local

  1. copy the runner script (a thin wrapper that sets some env vars) and make changes to the vars if needed

        cp local-dev.sh.example local-dev.sh
        chmod +x local-dev.sh
        vim local-dev.sh

  1. serve with hot reload at localhost:3000

        ./local-dev.sh

## Running with Docker in production

  1. clone the repo
  1. copy the runner script

        cp start-or-restart.sh.example start-or-restart.sh
        chmod +x start-or-restart.sh

  1. edit the runner script `start-or-restart.sh` to define the needed sensitive environmental variables

        vim start-or-restart.sh

  1. start the stack

        ./start-or-restart.sh
        # or if you need to force a rebuild of the app
        ./start-or-restart.sh --build



## Architecture
TODO add reasoning.

  - VueJS
  - Nuxt.js
  - Docker
  - docker-compose
  - Vuetify *and* Bootstrap-vue, so we have the best of both
